import React, { Component } from 'react'
import { Register, SignIn } from '.'

class AuthPage extends Component {
    render() {
        return (
        <div>
            register ?
            <Register />
            :
            <SignIn />
        </div>
        )
    }
}

export default AuthPage